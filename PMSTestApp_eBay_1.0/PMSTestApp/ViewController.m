//
//  ViewController.m
//  PMSTestApp
//
//  Created by amail_macbook_pro on 13. 3. 8..
//  Copyright (c) 2013년 amail. All rights reserved.
//

#import "ViewController.h"
#import "PMSIcon.h"
#import "PMSBar.h"
#import "PMS.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    PMSIcon *vIcon = [[PMSIcon alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 50.0f, 50.0f)];
    [vIcon setCenter:CGPointMake(self.view.center.x, self.view.center.y - 20.0f)];
    [self.view addSubview:vIcon];
    [vIcon release];
    
    UIButton *btnAuth = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 200.0f, 30.0f)];
    [btnAuth addTarget:self action:@selector(pressAuth:) forControlEvents:UIControlEventTouchUpInside];
    [btnAuth setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnAuth setTitle:@"start authorize" forState:UIControlStateNormal];
    [btnAuth setBackgroundImage:[UIImage imageNamed:@"btn_auth.png"]
                       forState:UIControlStateNormal];
    [btnAuth setCenter:CGPointMake(self.view.center.x, self.view.center.y + 140.0f)];
    [self.view addSubview:btnAuth];
    [btnAuth release];
    
    PMSBar *vBar = [[PMSBar alloc] initWithFrame:CGRectMake(0.0f, self.view.frame.size.height - 40.0f,
                                                            self.view.frame.size.width, 40.0f)];
    [self.view addSubview:vBar];
    [vBar release];
    
    UITextField *tfUserId = (UITextField *)[self.view viewWithTag:100];
    [tfUserId setText:[PMS getUserId]];
    
    UIButton *btnUserId = (UIButton *)[self.view viewWithTag:101];
    [btnUserId setBackgroundImage:[UIImage imageNamed:@"btn_auth.png"]
                         forState:UIControlStateNormal];
    
}



- (IBAction)pressAuth:(id)sender {
    [PMS authorize];
}

- (IBAction)pressUserId:(id)sender {
    UITextField *tfUserId = (UITextField *)[self.view viewWithTag:100];
    if(tfUserId.text.length > 0) {
        [PMS setUserId:tfUserId.text];
        [tfUserId resignFirstResponder];
    } else {
        UIAlertView *aView = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"User ID를 입력해주세요."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
        [aView show];
        [aView release];
    }
}

- (IBAction)pressAPNS:(id)sender {
    [PMS receivePush:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
