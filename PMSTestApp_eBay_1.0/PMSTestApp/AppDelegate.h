//
//  AppDelegate.h
//  PMSTestApp
//
//  Created by amail_macbook_pro on 13. 3. 8..
//  Copyright (c) 2013년 amail. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMS.h"

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate, PMSDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
