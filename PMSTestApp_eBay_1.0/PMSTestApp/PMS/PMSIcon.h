#import "PMSCommon.h"

@interface PMSIcon : PMSCommon

- (id)init;
- (id)initWithFrame:(CGRect)frame;
- (void)setPMSViewDelegate:(id<PMSViewDelegate>)argDelegate;

@end
