//
//  PMS Lib ver. 1.0.1
//
@protocol PMSDelegate<NSObject>
@optional
- (BOOL)showPMS;
@end

@interface PMS: NSObject {
    id<PMSDelegate> delegate;
}

@property(nonatomic,assign) id<PMSDelegate> delegate;

+ (void)initialize;
+ (BOOL)authorize;
+ (void)setPushToken:(NSData *)argTokenData;
+ (void)setMsgFlag:(BOOL)argIsMsg;
+ (void)setUserId:(NSString *)argUserId;
+ (NSString *)getUserId;
+ (void)setPMSDelegate:(id<PMSDelegate>)argDelegate;
+ (void)receivePush:(NSDictionary *)argDic;
+ (void)showMessageBox;
+ (void)closeMessageBox;

@end
