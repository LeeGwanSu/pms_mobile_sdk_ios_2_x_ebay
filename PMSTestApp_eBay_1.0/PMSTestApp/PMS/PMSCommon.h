#import <UIKit/UIKit.h>

@protocol PMSViewDelegate<NSObject>
@optional
- (BOOL)clickPMSView;
@end

@interface PMSCommon : UIView {
    id<PMSViewDelegate> delegate;
}
@property(nonatomic, assign) id<PMSViewDelegate> delegate;

- (id)init;
- (id)initWithFrame:(CGRect)frame;
- (void)initView:(CGRect)argFrame;
- (void)initCommon:(CGRect)argFrame;
- (void)setMessage;
- (void)setPMSViewDelegate:(id<PMSViewDelegate>)argDelegate;
- (void)showAppPush;

@end
