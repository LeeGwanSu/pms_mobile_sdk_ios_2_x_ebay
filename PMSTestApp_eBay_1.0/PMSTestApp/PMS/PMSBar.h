#import "PMSCommon.h"

@interface PMSBar : PMSCommon

- (id)init;
- (id)initWithFrame:(CGRect)frame;
- (void)setPMSViewDelegate:(id<PMSViewDelegate>)argDelegate;

@end
